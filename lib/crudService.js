const Services = require('../models/ServiceSchema');

const findOneServiceById = ((id)=> {
    const service = Services.findOne({"id": id}, (err, doc) => {
        return !err ? console.log(doc) : err;
    });
    return service;
});

const findOneServiceByObjectId = (id => {
    const service = Services.findOne({"_id": id}, (err, doc) => {
        return !err ? console.log(doc) : err;
    });
    return service;
})

const findAllServices = (()=> {
    const services = Services.find({}, (err, doc) => {
        return !err ? console.log(doc) : err;
    });
    return services;
});

const insertOneService = (data)=> {
    const newServices = new Services({
        typeOfService: data.typeOfService,
        name: data.name,
        imageUrl: data.imageUrl,
        punctuation: data.punctuation,
        longitude: data.longitude,
        latitude: data.latitude,
        street: data.street,
        zipCode: data.zipCode,
        city: data.city,
        testimonials: data.testimonials,
        opinions: data.opinions
    });
    newServices.save((err)=> {
        !err ?  console.log("Successfuly added a new Service!!") : console.log(err);
    })
}

const updateOneService = async (data) => {

    try {
    const service = await findOneServiceById(data.id);

        for( let prop in data) {
            data[prop] != undefined ? service[prop] = data[prop] : null;
        }
        service.save();
        return service;
    }
    catch (err) {
        console.log(err);
        throw err;
    }
};

const deleteOneService = async (id) => {

    try {
        const result = await Services.remove({"id": id});
        console.log(result);
    }
    catch (err) {
        console.log(err);
        throw err;
    }
};

module.exports = {
    findAllServices,
    findOneServiceById,
    findOneServiceByObjectId,
    insertOneService,
    updateOneService,
    deleteOneService
};