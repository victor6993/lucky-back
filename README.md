## Lucky Project Backend API

Esta es la parte del backend del projecto de Lucky, es un API Rest que incluye lo siguiente:

Desplegado en ->   https://lucky-back.herokuapp.com
** Todas las rutas partirán del anterior url.**

### Rutas:
- Autenticación:
  1. /login-association : Método POST, envía los datos de login de la asociación.
  2. /login : Método POST, envía los datos de login del usuario.
  3. /verify :  Método POST, envía el token de verificación para comprobar que el token guardado en local es válido.

- Usuarios:
  1. /user/all : Método GET, obtiene los datos de todos los usuarios.
  2. /user/ : Método POST, añade un nuevo usuario.
  3. /user/ : Método PUT, actualiza los datos de un usuario.
  4. /user/ : Método DELETE, borra un usuario.

- Animales:
  1. /animal/all : Método GET, obtiene los datos de todos los animales.
  2. /animal/:id : Método GET, obtiene los datos de un animal.
  3. /animal/ : Método POST, añade un nuevo animal.
  4. /animal/ : Método PUT, actualiza los datos de un animal.
  5. /animal/ : Método DELETE, borra un animal.

- Adopciones:
  1. /adoption/all : Método GET, obtiene los datos de todos las adopcicones.
  2. /adoption/ : Método GET, obtiene los datos de una adopción.
  3. /adoption/ : Método POST, añade una nueva adopción.
  4. /adoption/ : Método PUT, actualiza los datos de una adopción.
  5. /adoption/ : Método DELETE, borra una adopción.

- Servicios:
  1. /service/all : Método GET, obtiene los datos de todos los servicios.
  2. /service/:id : Método GET, obtiene los datos de un servicio.
  3. /service/ : Método POST, añade un nuevo servicio.
  4. /service/ : Método PUT, actualiza los datos de un servicio.
  5. /service/ : Método DELETE, borra un servicio.

- Opiniones:
  1. /opinion/all : Método GET, obtiene los datos de todos las opiniones.
  2. /opinion/:id : Método GET, obtiene los datos de una opinión.
  3. /opinion/ : Método POST, añade una nueva opinión.
  4. /opinion/ : Método PUT, actualiza los datos de una opinión.
  5. /opinion/ : Método DELETE, borra una opinión.

- Asociaciones:
  1. /association/all : Método GET, obtiene los datos de todos las  asociaciones.
  2. /association/ : Método POST, añade una nueva asociación.
  3. /association/ : Método PUT, actualiza los datos de una asociación.
  4. /association/ : Método DELETE, borra una asociación.