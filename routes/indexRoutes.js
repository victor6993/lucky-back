const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController')

router.route('/login-association')
    .post(authController.authAssociation, authController.getToken);

router.route('/login')
    .post(authController.authUser, authController.getToken);

router.route('/verify')
    .post(authController.verifyToken);

module.exports = router;
