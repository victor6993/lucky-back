const express = require('express');
const router = express.Router();
const associationController = require('../controllers/associationController');

router.route('/all').get(associationController.findAllAssociations);
router.route('/')
    .post(associationController.addAssociation)
    .put(associationController.updateAssociation)
    .delete(associationController.deleteAssociation);

module.exports = router;
