const express = require('express');
const router = express.Router();

const adoptionController = require('../controllers/adoptionController');

router.route("/all").get(adoptionController.findAllAdoptions)
router.route("/").get(adoptionController.findAdoption)
    .post(adoptionController.addAdoption)
    .put(adoptionController.updateAdoption)
    .delete(adoptionController.deleteAdoption);

module.exports = router;
