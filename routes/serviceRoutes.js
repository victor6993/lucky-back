const express = require('express');
const router = express.Router();

const servicesController = require('../controllers/serviceController');

router.route("/all").get(servicesController.findAllServices);
router.route("/:id").get(servicesController.findService);
router.route("/")
    .post(servicesController.addService)
    .put(servicesController.updateService)
    .delete(servicesController.deleteService);

module.exports = router;
